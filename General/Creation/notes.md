# Conference Name:

 * FOSS4G-Env
 * enviroFOSS
 * ecoFOSS
 * etc...

   check if anything similar exists, domain name availability

# Conference Committee:

Liz, Anthony, Barry + ?

# Conference Scope:

 Ecology? Conservation? Environmental Science

 UK? Global?

 Only geo- or all open source? "All open source" might expand the
field too much unless we focus on software rather than case
studies. Not keen.

# Topics:

Some thoughts about the types of talks we would like to see. This is
a very informal classification intended as a guide. A "Fantasy Conference"
sample agenda is included later in this document for more inspiration.

## Science Report: story about some science done using open-source.

 A Science Report's conclusion should be about the environment, rather than
the software. The software used should mostly be open source.

## Software Development: story about developing code for general eco-applications.

 A Software Development talk should have the rough form of a problem definition, 
an outline of current solutions, how the new software was designed, and how it 
fixes the problem. The conclusion should hopefully be a URL for downloading!

## Infrastructure: how an individual or team has used open-source tools for the win.

Infrastructure talks will focus on the wider usage of open-source by an individual or
within an organisation. A single scientific conclusion isn't needed, or this would
be a Science Report talk.



# Conference Format:

 * How big? 50? 100? more?
 * Where? University of Poppleton?
 *  When? January, Easter, Summer? Check clashes with GISRUK, OsGEO events
 
 Two days? One day of workshops, one day of presentations?

 Single stream everywhere? Think multi-stream workshops makes sense,
multiple presentations less so since they should be more general
interest, and maybe 3x16 person workshops in parallel and 1x50 person
presentation thread works well? Perhaps if we aim for 100 people then
run twice the streams.

# Fantasy Conference Agenda:

## Workshops

 * Workshop: Computing Habitat Suitability Maps in R
 * Workshop: Mapping Bird Migration in QGIS and The Web
 * Workshop: Using GRASS for Land Use Computation
 * Workshop: Get Away From Excel: Putting Your Data in a Spatial Database
 * Workshop: Drones for wildlife surveying (hands-on workshop)
 * Workshop: How to Include Open Source in your Research Grants

## Presentations

 * Presentation: Real-time Mapping of Badgers
 * Presentation: Lidar Reconstruction of Forest Nesting Sites
 * Presentation: Coastal Feature Cartography in QGIS
 * Presentation: Using R to Map Otter Distribution Changes on Mull
 * Presentation: A QGIS plugin for GPS tracker smoothing
 * Presentation: Migrating from proprietary GIS to Open Source in Our Lab
 * Presentation: Citizen Science Data Collection in the Cloud
 * Presentation: Toad habitat suitability and climate change

## Others

 * Lightning Talks
 * Posters
 * Unconference
 * Panel Discussion

# Finance

 * Support from OSGEO-UK funds
 * Minimal attendance fee
 * Sponsors?








